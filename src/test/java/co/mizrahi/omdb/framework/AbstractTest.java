package co.mizrahi.omdb.framework;

import co.mizrahi.omdb.OmdbApp;
import co.mizrahi.omdb.OmdbAppTests;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.ws.rs.core.MediaType;

@ActiveProfiles("test")
@PropertySource("classpath:application.properties")
@AutoConfigureMockMvc
@EnableWebMvc
@SpringBootTest(classes = {OmdbApp.class}, webEnvironment = org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractTest {

    @Autowired
    protected MockMvc mvc;

    protected static ObjectWriter ow = null;
    protected final static ObjectMapper om = new ObjectMapper();

    public AbstractTest() {
        SimpleModule module = new SimpleModule();
        om.registerModule(module);
        ow = om.writer().withDefaultPrettyPrinter();
    }

    protected MockHttpServletRequestBuilder setRequestBuilder(MockHttpServletRequestBuilder builder) {
        return builder.contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");
    }
}
