package co.mizrahi.omdb;

import co.mizrahi.omdb.framework.AbstractTest;
import co.mizrahi.omdb.model.ListMovies;
import co.mizrahi.omdb.model.Movie;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OmdbAppTests extends AbstractTest {

    private XmlMapper xmlMapper = new XmlMapper();

    @Test
    public void testGetMovie() throws Exception {
        var movieRequest = this.setRequestBuilder(get("/?i=tt3896198"));
        var actions = this.mvc.perform(movieRequest).andExpect(status().is(200));
        String stringXML = actions.andReturn().getResponse().getContentAsString();
        Movie movie = xmlMapper.readValue(stringXML, Movie.class);
        assertEquals("Guardians of the Galaxy Vol. 2", movie.getTitle());
    }

    @Test
    public void testGetSearch() throws Exception {
        var movieRequest = this.setRequestBuilder(get("/?s=Omen&page=1"));
        var actions = this.mvc.perform(movieRequest).andExpect(status().is(200));
        String stringXML = actions.andReturn().getResponse().getContentAsString();
        ListMovies movies = xmlMapper.readValue(stringXML, ListMovies.class);
        assertEquals(42, movies.getTotalResults());
    }
}
