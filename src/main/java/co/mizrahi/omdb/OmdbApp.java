package co.mizrahi.omdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class OmdbApp {

    public static void main(String[] args) {
        SpringApplication.
                run(OmdbApp.class, args);
    }
}
