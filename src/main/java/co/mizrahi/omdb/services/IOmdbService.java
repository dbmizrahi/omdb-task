package co.mizrahi.omdb.services;

import co.mizrahi.omdb.model.*;

public interface IOmdbService {
    Movie getByIdOrTitle(SearchFilter filter);
    ListMovies getBySearch(SearchFilter filter);
    SearchFilter getFilter(String i, String t, String type, Integer y, String plot, Integer page, String s);
}
