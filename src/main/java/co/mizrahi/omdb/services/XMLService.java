package co.mizrahi.omdb.services;

import co.mizrahi.omdb.model.Movie;
import co.mizrahi.omdb.utils.MyException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

/**
 * service for XML manipulations
 */
@Service
public class XMLService {

    /**
     * save the movie data to XML file
     * @param movie
     * @param path
     */
    public void saveMovie(Movie movie, String path) {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            FileOutputStream outStream = new FileOutputStream(
                    new File(path + movie.getTitle() + "." +
                    movie.getImdbID() + ".xml")
            );
            transformer.transform(
                    new DOMSource(
                            convertStringToXMLDocument(
                                    movieToXML(movie)
                            )
                    ),
                    new StreamResult(outStream));
        } catch (Exception e) {
            throw new MyException("Save operation failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * convert XML string to XML Document object
     * @param xmlString
     * @return Document
     */
    public Document convertStringToXMLDocument(String xmlString)
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            return builder.parse(new InputSource(new StringReader(xmlString)));
        }
        catch (Exception e) {
            throw new MyException("Conversion of string to XML document failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * convert Movie to XML string
     * @param movie
     * @return string
     */
    public String movieToXML(Movie movie) {
        try {
            XmlMapper xmlMapper = new XmlMapper();
            return xmlMapper.writeValueAsString(movie);
        } catch (JsonProcessingException e) {
            throw new MyException("Conversion of object to XML failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * read XML file to Movie object
     * @param fileName
     * @return movie
     */
    public Movie readXmlToMovie(String fileName) {
        File xmlFile = new File(fileName);
        try {
            XmlMapper xmlMapper = new XmlMapper();
            String xml = inputStreamToString(new FileInputStream(xmlFile));
            return xmlMapper.readValue(xml, Movie.class);
        }
        catch (IOException e) {
            throw new MyException("File reading failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }

    /**
     * find the filename of the Movie data if exists by part of this name
     * @param fileNamePart Id or Title
     * @param path
     * @return string
     */
    public String findSavedMovie(String fileNamePart, String path) {
        try {
            List<Path> list = Files.list(Path.of(path)).filter(f -> f.getFileName()
                    .toString().
                            contains(fileNamePart))
                    .collect(Collectors.toList());
            if (list.isEmpty()) return null;
            return list.get(0).getFileName().toString();
        } catch (IOException e) {
            return null;
        }
    }
}
