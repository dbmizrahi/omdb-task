package co.mizrahi.omdb.services;

import co.mizrahi.omdb.model.*;
import co.mizrahi.omdb.utils.MyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Objects.isNull;

/**
 * Service for request handling only
 */
@Service
public class OmdbService implements IOmdbService {

    @Value("${omdb.search.uri}")
    private String baseUrl; // Receive the baseUrl string from  application.properties
    @Value("${movies.path}")
    private String path; // Receive the string of path from  application.properties to the folder
                         // where we store the xml files with movies data

    private final XMLService xmlService;
    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    public OmdbService(XMLService xmlService) {
        this.xmlService = checkNotNull(xmlService);
    }

    /**
     * Receive set of parameters for movie searching
     * @param filter SerchFilter
     * @return Movie DTO
     */
    @Override
    public Movie getByIdOrTitle(SearchFilter filter) {
        // get uri with all request params for sending it to OMDB
        URI uri = getUri(filter, false);
        // define the id or title as a part of the filename for searching it
        String fileNamePart = isNull(filter.getI()) ? filter.getT() : filter.getI();
        // searching for the filename in specified folder
        String filename = this.xmlService.findSavedMovie(fileNamePart, path);
        Movie movie;
        // read, build the Movie object and return it if the file exists
        if (!isNull(filename)) {
            movie = this.xmlService.readXmlToMovie(path + filename);
            movie.setFromFS(true);
        }
        // read the data from OMDB and return if the file wasn't found
        else {
            ResponseEntity<Movie> responseEntity = this.restTemplate.getForEntity(uri, Movie.class);
            checkStatus(responseEntity.getStatusCode());
            movie = responseEntity.getBody();
            this.xmlService.saveMovie(movie, path);
        }
        // make the plot shorter if filter.getPlot().equals("short")
        // just the first sentence
        if (!isNull(filter.getPlot()) && filter.getPlot().equals("short")) {
            String newPlot = movie.getPlot().substring(0, movie.getPlot().indexOf('.') + 1);
            movie.setPlot(newPlot);
        }
        return movie;
    }

    /**
     * Receive set of parameters for ListMovies searching
     * @param filter SerchFilter
     * @return ListMovies
     */
    @Override
    public ListMovies getBySearch(SearchFilter filter) {
        URI uri = getUri(filter, true);
        ResponseEntity<ListMovies> responseEntity = this.restTemplate.getForEntity(uri, ListMovies.class);
        checkStatus(responseEntity.getStatusCode());
        return responseEntity.getBody();
    }

    /**
     * Receiving the request params and building the SearchFilter from it
     * @param i IMDB Id
     * @param t Title for request by Id or Title
     * @param type Type of media
     * @param y Year of realise
     * @param plot flag (short, full) for plot size
     * @param page number of the page in pageable response of ListMovies
     * @param s Title for request by search
     * @return SearchFilter
     */
    @Override
    public SearchFilter getFilter(String i, String t, String type, Integer y, String plot, Integer page, String s) {
        return SearchFilter.builder()
                .i(i)
                .t(t)
                .type(type)
                .y(y)
                .plot(plot)
                .page(page)
                .s(s)
                .build();
    }

    /**
     * build URI object for feature request to OMDB
     * @param filter
     * @param search
     * @return URI
     */
    private URI getUri(SearchFilter filter, boolean search) {
        StringBuilder sb = new StringBuilder();
        sb.append(baseUrl);
        buildUri(filter, sb, search);
        String urlStr = sb.toString();
        URI url;
        try {
            url = new URI(urlStr);
        } catch (URISyntaxException e) {
            throw new MyException("Incorrect request parameters", HttpStatus.BAD_REQUEST);
        }
        return url;
    }

    /**
     * build uri string
     * @param filter
     * @param sb
     * @param search
     */
    private void buildUri(SearchFilter filter, StringBuilder sb, boolean search) {
        if (search) {
            if (isNull(filter.getS()))
                throw new MyException("Movie Title must be presented", HttpStatus.BAD_REQUEST);
            sb.append("s=").append(filter.getS());
        } else {
            if (isNull(filter.getI()) && isNull(filter.getT()))
                throw new MyException("IMDB movie Id or Title must be presented", HttpStatus.BAD_REQUEST);
            if (!isNull(filter.getI())) sb.append("i=").append(filter.getI());
            if (!isNull(filter.getT())) sb.append("&").append("t=").append(filter.getT());
        }
        sb.append("&").append("plot=full");
        if (!isNull(filter.getType())) sb.append("&").append("type=").append(filter.getType());
        if (!isNull(filter.getPage())) sb.append("&").append("page=").append(filter.getPage());
        if (!isNull(filter.getY())) sb.append("&").append("y=").append(filter.getY());
    }

    /**
     * Check if response HttpStatus not OK
     * @param status
     */
    private void checkStatus(HttpStatus status) {
        if (!status.equals(HttpStatus.OK))
            throw new MyException("Response failed", status);
    }
}
