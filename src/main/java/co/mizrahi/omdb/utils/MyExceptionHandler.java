package co.mizrahi.omdb.utils;

import lombok.extern.java.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.net.URISyntaxException;

@ControllerAdvice
@Log
public class MyExceptionHandler {

    @ExceptionHandler({MyException.class})
    @ResponseBody
    @SuppressWarnings("unchecked")
    ResponseEntity handleBadRequest(HttpServletRequest req, MyException ex) {
        return new ResponseEntity(new MyExceptionInfo(ex, req.getServletPath()), ex.getHttpStatus());
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MyExceptionInfo handle(HttpServletRequest req, MethodArgumentNotValidException ex) {
        log.warning("Caught exception: " + ex.getMessage());
        return new MyExceptionInfo(new MyException(ex.getMessage(), HttpStatus.BAD_REQUEST), req.getServletPath());
    }

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MyExceptionInfo handle(HttpServletRequest req, ConstraintViolationException ex) {
        log.warning("Caught exception: " + ex.getMessage());
        return new MyExceptionInfo(new MyException(ex.getMessage(), HttpStatus.BAD_REQUEST), req.getServletPath());
    }

    @ExceptionHandler({ ClassNotFoundException.class })
    @ResponseBody
    @SuppressWarnings("unchecked")
    ResponseEntity handleBadRequest(HttpServletRequest req, ClassNotFoundException ex) {
        log.warning("Caught exception: " + ex.getMessage());
        return new ResponseEntity(
                new MyExceptionInfo(new MyException("Invalid Request", HttpStatus.BAD_REQUEST), req.getServletPath()),
                HttpStatus.BAD_REQUEST);
    }
}
