package co.mizrahi.omdb.utils;

import org.springframework.http.HttpStatus;

import javax.ws.rs.BadRequestException;

public class MyException extends BadRequestException {

    private String msg;
    private HttpStatus status;

    @SuppressWarnings("WeakerAccess")
    public HttpStatus getHttpStatus() { return this.status; }

    public MyException(String msg, HttpStatus status) {
        this.msg = msg;
        this.status = status;
    }
}
