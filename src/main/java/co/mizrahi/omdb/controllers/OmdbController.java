package co.mizrahi.omdb.controllers;

import co.mizrahi.omdb.model.*;
import co.mizrahi.omdb.services.IOmdbService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * REST controller for handling REST requests
 */
@RestController
public class OmdbController {

    private final IOmdbService service;

    public OmdbController(IOmdbService service) {
        this.service = checkNotNull(service);
    }

    /**
     * method for dynamically build get request
     * @param i IMDB Id
     * @param t Title for request by Id or Title
     * @param type Type of media
     * @param y Year of realise
     * @param plot flag (short, full) for plot size
     * @param page number of the page in pageable response of ListMovies
     * @param s Title for request by search
     * @return child of ResponseOmdb
     */
    @GetMapping(produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseOmdb get(@RequestParam(required = false) String i,
                            @RequestParam(required = false) String t,
                            @RequestParam(required = false) String type,
                            @RequestParam(required = false) Integer y,
                            @RequestParam(required = false) String plot,
                            @RequestParam(required = false) Integer page,
                            @RequestParam(required = false) String s) {
        SearchFilter filter = this.service.getFilter(i, t, type, y, plot, page, s);
        // choose the method handler
        if (!isNullOrEmpty(s)) return this.service.getBySearch(filter);
        return this.service.getByIdOrTitle(filter);
    }
}
