package co.mizrahi.omdb.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * abstract parent for response DTO
 */
@XmlRootElement
@NoArgsConstructor
@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class ResponseOmdb {
    @JsonProperty("Response")
    Boolean Response; // true if the response received successful
    @JsonProperty("Error")
    String Error; // Error message if Response field is false
    @JsonProperty("fromFS")
    Boolean fromFS = false; // Is response received from filesystem?
}
