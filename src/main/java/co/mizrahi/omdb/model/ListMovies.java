package co.mizrahi.omdb.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * page of list of Movies short data
 */
@XmlRootElement
@NoArgsConstructor
@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListMovies extends ResponseOmdb {

    @JsonProperty("Search")
    List<Movie> Search; // list of movies short data

    @JsonProperty("totalResults")
    Integer totalResults; // total quantity of movies was found
}
