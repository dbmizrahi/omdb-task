package co.mizrahi.omdb.model;

import lombok.*;

/**
 * Search Filter Model
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter @Getter @Builder
public class SearchFilter {
    String i; // A valid IMDb ID (e.g. tt1285016)
    String t; // Movie title to search for.
    String type; // Type of result to return.
    Integer y; // 	Year of release.
    String plot; // Return short or full plot.
    String s; // Movie title to search for.
    Integer page; // Number of the page in ListMovie search
}
