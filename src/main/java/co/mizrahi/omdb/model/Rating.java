package co.mizrahi.omdb.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@AllArgsConstructor
@NoArgsConstructor
@Getter @Setter @Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Rating {

    @JsonProperty("Source")
    String Source;// Source: "Internet Movie Database",

    @JsonProperty("Value")
    String Value;// Value: "7.6/10"
}
