package co.mizrahi.omdb.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * movie data POJO
 */
@XmlRootElement
@NoArgsConstructor
@Getter @Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Movie extends ResponseOmdb {

    @JsonProperty("Title")
    String Title; // movie title

    @JsonProperty("Year")
    Integer Year; // year of production

    @JsonProperty("Rated")
    String Rated; // rated

    @JsonProperty("Released")
    String Released; // full date of realise

    @JsonProperty("Runtime")
    String Runtime; // duration

    @JsonProperty("Genre")
    String Genre; // genre

    @JsonProperty("Director")
    String Director; // director's name

    @JsonProperty("Writer")
    String Writer; // string with list of writers

    @JsonProperty("Actors")
    String Actors; // string with list of actors

    @JsonProperty("Plot")
    String Plot; // movie's plot

    @JsonProperty("Language")
    String Language; // spoken language of the movie

    @JsonProperty("Country")
    String Country; // country of production

    @JsonProperty("Awards")
    String Awards; // string with list of obtained awards

    @JsonProperty("Poster")
    String Poster; // link to the movie's poster

    @JsonProperty("Ratings")
    List<Rating> Ratings; // list of ratings

    @JsonProperty("Metascore")
    String Metascore; // string of meta score value

    @JsonProperty("imdbRating")
    Double imdbRating; // IMDB rating of the movie

    @JsonProperty("imdbVotes")
    String imdbVotes; // string of number of total IMDB votes

    @JsonProperty("imdbID")
    String imdbID;

    @JsonProperty("Type")
    String Type; // movie's type

    @JsonProperty("DVD")
    String DVD; // date of realised on DVD

    @JsonProperty("BoxOffice")
    String BoxOffice; // string of Box Office amount

    @JsonProperty("Production")
    String Production; // name of the production company

    @JsonProperty("Website")
    String Website; // movie's web site
}
